var indexBy = function(arr,key){
    var ret = {};
    arr.forEach(function(item){
        // if (item[key]) ret[item[key]] = item;
        if (has(item,key)) ret[item[key]] = item;
    })
    return ret;
}

var fillModelsGalleries = function(product){
    //fill galleries of products if there are too few images
    //we have to fill all models... fill model i with images from model j
    // return product;

    product.models.forEach(function(model,i){
        if(model){
            var firstSizeModelIndex = getFirstSizeModel(product.models,model);
            var firstSizeModel = product.models[firstSizeModelIndex];
            model.gallery = angular.copy(firstSizeModel.gallery);
            if (!model.gallery) model.gallery = [];

            for (var imageIndex=0;imageIndex<=4;imageIndex++){
                //try to get second,third,fourth images from other models (skip the first, because they're all the same)
                if (model.gallery.length<6) product.models.forEach(function(modelFill,j){
                    if ((model.gallery.length>=6) || (firstSizeModelIndex==j)) return;
                    else if (!modelFill.gallery[imageIndex]) return;
                    else if (model.gallery.indexOf(modelFill.gallery[imageIndex])==-1) {
                        model.gallery.push(modelFill.gallery[imageIndex]);
                    }
                })
            }
        }
    })

    return product;
}

var getFirstSizeModel = function(models,model){
    //input: a product model. Search the first size of the same color.

    var firstModelIndex = null;
    models.forEach(function(item,i){
        if (typeof firstModelIndex=="number") return;
        if (item.color.id==model.color.id && item.gallery && item.gallery.length){
            console.log(item.gallery);
            firstModelIndex = i;
        }
    })
    return firstModelIndex;
}

var orderColors = function(product){
    //sort colors based on square (configurable) images order
    var colors = [];
    var colorsIndex = [];

    if ((!product.color) || (!product.color.forEach)) return product;

    product.gallery.forEach(function(image){
        var filename = image.replace(/^.*\/|\.[^.]*$/g, '');
        if(!isNaN(filename.substring(0,2))){ // NEW SKU
            var galleryColorId = parseInt(filename.slice(7,9));
        }else{
            var galleryColorId = parseInt(filename.slice(9,11));
        }  
        var galleryColorName = config.colors[galleryColorId];
        if (galleryColorName) galleryColorName = galleryColorName.string;
        else return;

        product.color.forEach(function(color){
            if ((color.admin_label==galleryColorName) && (colorsIndex.indexOf(color.id)==-1)){
                colors.push(color);
                colorsIndex.push(color.id);
            }
        })
    })

    //fill orphan colors (those which don't have square images)
    product.color = colors.concat(product.color.filter(function(color){
        if (colorsIndex.indexOf(color.id)==-1) return true;
        else return false;
    }))

    return product;
}

function getUrlImageProduct(product,colors){
    var color;

    if(product.favorite_color){
        color = convertColorSkuToMagento(product.favorite_color,colors)
        console.log("color="+JSON.parse(color));
        var pos = changeColor(color,product);
        console.log("pos="+pos);
        if(!product.gallery[pos]) return product.gallery[0];
        else return product.gallery[pos];
    }
    else if (product && (product.color) && (product.color.length)) {
        //take the first color available
        color = product.color[0];
        console.log("color="+JSON.parse(color));
        var pos = changeColor(color,product);
        console.log("pos="+pos);
        if(!product.gallery[pos]) return product.gallery[0];
        else return product.gallery[pos];
        
    } else if(product && product.gallery && product.gallery.length){
        return product.gallery[0];
    }else {
        return product.gallery[0];
    }
}

function changeColor(color,product){
    if (color) {
        return searchGalleryForColor(color,product);
    }
}

function searchGalleryForColor(color,product){
    console.log("searchGalleryForColor="+JSON.parse(color));
    //color: color object || color string/number
    if (typeof color == "string") color = parseInt(color);
    var colorIndex = null;
    if (color){
        product.gallery.forEach(function(galleryItem,i){
            if (typeof colorIndex=="number") return;
            var filename = galleryItem.replace(/^.*\/|\.[^.]*$/g, '');
            var galleryColorId;
            if(isNumber(filename.substring(0,2))){	// NEW SKU
                if(isNumber(filename.slice(7,11))){
                    galleryColorId = parseInt(filename.slice(7,11));
                } else {
                    galleryColorId = parseInt(filename.slice(7,9));
                }
            }else{	// OLD SKU
                if(isNumber(filename.slice(9,13))){
                    galleryColorId = parseInt(filename.slice(9,13));
                } else {
                    galleryColorId = parseInt(filename.slice(9,11));
                }
            }
            var galleryColor;
            if(!isNumber(galleryColorId)){
                galleryColor = galleryColorId;
            }else {
                galleryColor = config.colors[galleryColorId];
            }

            if ((typeof color == "number") && (color==galleryColorId)) colorIndex = i;
            else{
                var galleryColors = config.colors[galleryColorId];
                if (galleryColors) galleryColors.forEach(function(galleryColor){
                    if (galleryColor.string==color.admin_label) colorIndex = i;
                })
            }
        })
    }
    return colorIndex || 0;
}

function isNumber (o) {
    return ! isNaN (o-0) && o !== null && o !== "" && o !== false;
}


function convertColorSkuToMagento(color,colors){
    //color is a SKU color. Search colors from magento and get the magento ID
    var colorsObj = colors[parseInt(color)];

    if ((!colorsObj) || (!colorsObj.length)) return null;

    colors.forEach(function(magentoColor){
        if (ret) return;
        colorsObj.forEach(function(colorObj){
            if (magentoColor.admin_label==colorObj.string) ret = magentoColor;
        });
    });
}


var getCategoryName = function(categories,id){

    categories.forEach(function(cat){
        if(cat.id == id){
            return cat.name;
        }
    })

    return "";
}

var colors = {
          '0':  [
            { code: 0, string: 'Black' }
            // { code: 0, string: 'BlackDots' }
          ],
          '1':  [
            { code: 1, string: 'White' }
            // { code: 1, string: 'WhiteFlag' }
          ],
          '2':  [
            { code: 2, string: 'Grey' },
            { code: 2, string: 'LightGrey' }
          ],
          '3':  [
            { code: 3, string: 'MilitaryGreen' }
          ],
          '4':  [
            { code: 4, string: 'Beige' }
          ],
          '5':  [
            { code: 5, string: 'Navy' },
            { code: 5, string: 'Slate' }
          ],
          '6':  [
            { code: 6, string: 'Red' }
          ],
          '7':  [
            { code: 7, string: 'Blue' }
          ],
          '9':  [
            { code: 9, string: 'Indigo' }
          ],
          '11': [
            { code: 11, string: 'Green' }
          ],
          '13': [
            { code: 13, string: 'Bordeaux' }
          ],
          '14': [
            { code: 14, string: 'Brown' },
            { code: 14, string: 'Khaki' }
          ],
          '15': [{ code: 15, string: 'Yellow' }],
          '16': [
            { code: 16, string: 'LightGreyMelange' }
          ],
          '27': [
            { code: 27, string: 'Petrol' }
          ],
          '47': [
            { code: 47, string: 'Blue' }
          ],
          '48': [
            { code: 48, string: 'Black'},
            { code: 48, string: 'DarkGrey'}
          ],
        }


module.exports = {
    indexBy: indexBy,
    fillModelsGalleries: fillModelsGalleries,
    getFirstSizeModel: getFirstSizeModel,
    orderColors: orderColors,
    getUrlImageProduct: getUrlImageProduct,
    getCategoryName:getCategoryName
}