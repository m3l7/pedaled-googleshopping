var request = require('request');
var rp = require('request-promise');
var conf = require('rc')('instance');
var utils = require('./utils.js');

var endpoint = conf.magentoapi.endpoint;
var productsUrl = endpoint+conf.magentoapi.products;
var productUrl = endpoint+conf.magentoapi.product;
var storesUrl = endpoint+conf.magentoapi.stores;
var categoriesUrl = endpoint+conf.magentoapi.categories;

var sizesUrl = endpoint+conf.magentoapi.sizes;
var colorsUrl = endpoint+conf.magentoapi.colors;

var getProducts = function(){
    return rp({
        uri: productsUrl,
        json: true // Automatically parses the JSON string in the response)
    });
}

var getCategories = function(){
    return rp({
        uri: categoriesUrl,
        json: true // Automatically parses the JSON string in the response)
    });
}

var getStores = function(){
    return rp({
        uri: storesUrl,
        json: true // Automatically parses the JSON string in the response)
    });
}

var getColors = function(){
    return rp({
        uri: colorsUrl,
        json: true // Automatically parses the JSON string in the response)
    });
}

var getSizes = function(){
    return rp({
        uri: sizesUrl,
        json: true // Automatically parses the JSON string in the response)
    });
}

var getProduct = function(idProduct,storeId){
    return rp({
        uri: productUrl+"/"+idProduct+"?storeId="+storeId,
        json: true // Automatically parses the JSON string in the response)
    });
}



var getProductComplete = function(idProduct,storeId){
    var prodAttributesOperations = [getColors(),getSizes(),getProduct(idProduct,storeId)]
    return Promise.all(prodAttributesOperations)
    .then(function(data){

        var product = data[2];
        var colorsIndex = utils.indexBy(data[0],"id");
        var sizesIndex = utils.indexBy(data[1],"id");

        product.models.forEach(function(model){
            model.color = colorsIndex[model.color];
            model.size = sizesIndex[model.size];
        })

        if (typeof product.color == "object"){
            var colors = [];
            if (product.color.length) product.color.forEach(function(colorItem){
                if (typeof colorItem == "number") colors.push(colorsIndex[colorItem]);
            })
            if (colors.length) product.color = colors;                            

        }
        else if (typeof product.color == "number") product.color = colorsIndex[product.color];

        if (typeof product.size == "object"){
            var sizes = [];
            if (product.size.length) product.size.forEach(function(sizeItem){
                if (typeof sizeItem=="number") sizes.push(sizesIndex[sizeItem]);
            })
            if (sizes.length) product.size = sizes;                            

        }
        else if (typeof product.size == "number") product.size = sizesIndex[product.size];

        product = utils.orderColors(product);

        return product;
    })
    .then(utils.fillModelsGalleries)
    .then(function(product){
        return product;
    });

}



module.exports = {
    getStores: getStores,
    getProducts: getProducts,
    getProductComplete: getProductComplete,
    getCategories: getCategories
};