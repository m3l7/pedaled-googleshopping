var CronJob = require('cron').CronJob;
var serviceMagento = require('./serviceMagento');
var xml = require('xml');
var utils = require('./utils.js');
var fs = require('fs');
var jstoxml = require('jstoxml');

var startCron = function(){

    new CronJob({
        cronTime: '0 4 * * *', //ogni giorno alle 4
        onTick: function() {
            buildXmlFeed();
        },
        start: true
    });
    buildXmlFeed();
}

var buildXmlFeed = function(){

  var initOperations = [serviceMagento.getStores(),serviceMagento.getProducts(),serviceMagento.getCategories()]
  Promise.all(initOperations)
  .then(function(results){
      if(results && results.length){
        var stores = results[0];
        var products = results[1];
        var categories = results[2];
        var feedList = [];
        stores.forEach(function(store){
            var feed = {
                _attrs: {
                        "xmlns:g": "http://base.google.com/ns/1.0",
                        version: "2.0",
                        "data-livestyle-extension": "available"
                    },
                _content:{
                    channel:{
                        title: "Feed products for store "+store.code,
                        description: "All products availability, price for store "+store.code,
                        link: "https://pedaled.com/#!/?store="+store.code,
                        item: []
                    }
                },
                _name: "rss"
            };
            // var asyncGetProdSimple = [];
            // products.forEach(function(prod){
            //     asyncGetProdSimple.push(serviceMagento.getProductComplete(prod.id,store.id));     
            // })

            // Promise.all(asyncGetProdSimple)
            // .then(function(productsComplete){
            //     // prodotto simple con tutti gli attributi riferito a specifico store
            //     if(!productsComplete || !productsComplete.length){
            //         console.warn("ProductsComplete null");
            //         return;
            //     }
                
                products.forEach(function(productComplete){

                    // getUrlImageProduct()
                    // .then(function(urlImage){
                    feed._content.channel.item.push({
                        "g:id": productComplete.sku,
                        title: productComplete.name,
                        description: productComplete.description.replace("\n",""),
                        "g:link": "https://pedaled.com/#!/products/"+productComplete.urlKey+"?store="+store.code,
                        "g:image_link": productComplete.gallery[0],
                        "additional_image_link": productComplete.gallery,
                        availability: getAvailabilityLabel(productComplete),
                        "g:price": productComplete.price,
                        "g:priceCurrency": store.currency,
                        "g:sale_price": productComplete.final_price || "",
                        // "g:unit_pricing_measure": (productComplete.models[0].weight +" g") || null,
                        "g:google_product_category": utils.getCategoryName(categories, productComplete.categories[0]),
                        "g:condition": "new",
                        "g:material": productComplete.materials,
                        "g:shipping": "9 "+store.currency
                    })
                    // })
                    
                // })

                
            })
            

            var xmlFeedString = jstoxml.toXML(feed);
                fs.writeFile('./googleshopping-products-store-'+store.code+'.xml', xmlFeedString, err => {
                    if (err) return console.log(err);
                    console.log('Done @ '+new Date());
                });
        });
        
      }
  })
  .catch(function(error){
      console.error(error);
  })

}

var getAvailabilityLabel = function(product){

    if(product.models && product.models.length){
        product.models.forEach(function(model){
            if(model.quantity>0) return 'in_stock'; // se almeno un simple ha disponibilità
        })
        return 'out_of_stock';
    }else{
        return 'out_of_stock';
    }

}

var manageStoresResult = function(error,stores){

    if(error){
        console.error(error);
        return;
    }

    if(stores && stores.length){

        stores.forEach(function(store) {
            serviceMagento.getStores(manageStoresResult);
        });

    }

}

var manageProductsResult = function(error,stores){

}

module.exports = {
    startCron: startCron
};